package com.johnny.perfectgraduate.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.johnny.perfectgraduate.R;
import com.johnny.perfectgraduate.base.BaseActivity;

public class StudentSocietyActivity extends BaseActivity implements View.OnClickListener {

    TextView ui_submit;
    ImageView ui_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_society);

        showAlertDialogWithTitle(getString(R.string.almost_there), getString(R.string.msg_2));

        loadLayout();

    }

    private void loadLayout() {

        ui_submit = (TextView) findViewById(R.id.tv_submit);
        ui_submit.setOnClickListener(this);

        ui_close = (ImageView) findViewById(R.id.imv_back);
        ui_close.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tv_submit :
                gotoTerms();
                break;

            case R.id.imv_back :
                finish();
                break;
        }
    }

    private void gotoTerms() {

        Intent intent = new Intent(this, TermActivity.class);
        startActivity(intent);
    }
}
