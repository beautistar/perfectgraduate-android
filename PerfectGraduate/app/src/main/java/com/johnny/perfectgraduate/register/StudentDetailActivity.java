package com.johnny.perfectgraduate.register;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.johnny.perfectgraduate.R;
import com.johnny.perfectgraduate.base.BaseActivity;

public class StudentDetailActivity extends BaseActivity implements View.OnClickListener {

    ImageView ui_back;
    TextView ui_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_detail);

        showAlertDialogWithTitle(getString(R.string.alert_title), getString(R.string.alert_msg1));
//        showStartAlert("a", "d");

        loadLayout();
    }

    private void loadLayout() {

        ui_back = (ImageView) findViewById(R.id.imv_back);
        ui_back.setOnClickListener(this);

        ui_next = (TextView) findViewById(R.id.tv_next);
        ui_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.imv_back :

                finish();
                break;

            case R.id.tv_next:
                gotoStdSociety();
                break;
        }
    }

    private void gotoStdSociety() {

        Intent intent = new Intent(this, StudentSocietyActivity.class);
        startActivity(intent);
    }


}
