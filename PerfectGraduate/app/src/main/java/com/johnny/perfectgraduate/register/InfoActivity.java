package com.johnny.perfectgraduate.register;

import android.content.Intent;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.johnny.perfectgraduate.R;
import com.johnny.perfectgraduate.base.BaseActivity;

import java.util.Calendar;

public class InfoActivity extends BaseActivity implements View.OnClickListener {

    TextView ui_year, ui_tmp_login;
    ImageView ui_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        loadLayout();

    }

    private void loadLayout() {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        ui_year = (TextView) findViewById(R.id.tv_year);
        ui_year.setText(getString(R.string.year) + " " + String.valueOf(year));

        ui_tmp_login = (TextView) findViewById(R.id.tv_tmp_login);
        ui_tmp_login.setOnClickListener(this);

        ui_close = (ImageView) findViewById(R.id.imv_close);
        ui_close.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.imv_close:
                finish();
                break;

            case R.id.tv_tmp_login :

                gotoTmpLogin();
        }
    }

    private void gotoTmpLogin() {

        Intent loginIntent = new Intent(this, StudentDetailActivity.class);
        startActivity(loginIntent);
        finish();
    }
}
