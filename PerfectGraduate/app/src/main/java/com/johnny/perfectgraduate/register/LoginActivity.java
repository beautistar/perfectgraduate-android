package com.johnny.perfectgraduate.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.johnny.perfectgraduate.R;
import com.johnny.perfectgraduate.base.BaseActivity;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    TextView ui_fbLogin, ui_tmpLogin;
    ImageView ui_i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loadLayout();
    }

    private void loadLayout() {

        ui_fbLogin = (TextView) findViewById(R.id.tv_fb_login);

        ui_tmpLogin = (TextView) findViewById(R.id.tv_tmp_login);
        ui_tmpLogin.setOnClickListener(this);

        ui_i = (ImageView) findViewById(R.id.imv_i);
        ui_i.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.tv_tmp_login) {
            //goto temporary login

            gotoTmpLoginStudentDetail();
        } else {
            // goto i
            gotoI();
        }
    }

    private void gotoI() {

        Intent iIntent = new Intent(this, InfoActivity.class);
        startActivity(iIntent);

    }

    private void gotoTmpLoginStudentDetail() {

        Intent stdDetailIntent = new Intent(LoginActivity.this, StudentDetailActivity.class);
        startActivity(stdDetailIntent);
    }
}
