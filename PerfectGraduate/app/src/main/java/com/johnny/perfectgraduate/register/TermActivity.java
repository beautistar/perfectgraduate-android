package com.johnny.perfectgraduate.register;

import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.johnny.perfectgraduate.R;
import com.johnny.perfectgraduate.base.BaseActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class TermActivity extends BaseActivity implements View.OnClickListener {

    TextView ui_terms;
    ImageView ui_close, ui_check;

    boolean checked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);

        ui_terms = (TextView) findViewById(R.id.tv_terms);

        ui_close = (ImageView) findViewById(R.id.imv_back);
        ui_close.setOnClickListener(this);

        ui_check = (ImageView) findViewById(R.id.imv_check);
        ui_check.setOnClickListener(this);

        checked = false;

        setText();
    }

    public void setText() {

        String data = null;

        int resId = R.raw.terms;

        InputStream inputStream = getResources().openRawResource(resId);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i;
        try {
            i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }

            data = new String(byteArrayOutputStream.toByteArray());
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ui_terms.setText(data);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imv_back) {
            finish();
        } else if (v.getId() == R.id.imv_check) {

            if (checked) {
                ui_check.setImageResource(R.drawable.agree_checked);
                checked = !checked;
            } else {
                ui_check.setImageResource(R.drawable.agree_unchecked);
                checked = !checked;
            }
        }
    }
}
